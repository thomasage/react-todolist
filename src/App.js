import React, { useState } from 'react'
import { useLiveQuery } from 'dexie-react-hooks'
import db from './database'
import { v4 as uuidv4 } from 'uuid'

export default function App () {
  const [name, setName] = useState('')
  const tasks = useLiveQuery(() => db.tasks.orderBy('name').toArray())
  if (!tasks) {
    return null
  }

  function handleSubmit (event) {
    event.preventDefault()
    if (name.length === 0) {
      return
    }
    db.tasks.add({
      name: name,
      uuid: uuidv4()
    })
    setName('')
  }

  function onTaskDelete (uuid) {
    db.tasks.delete(uuid)
  }

  return (
      <div className="container">
          <form onSubmit={handleSubmit} className="mt-5">
              <div className="d-flex flex-row align-items-center">
                  <input
                      type="text"
                      className="form-control flex-grow-1 me-3"
                      value={name}
                      onChange={event => setName(event.target.value)}
                  />
                  <button type="submit" className="btn btn-success text-nowrap">Add this task</button>
              </div>
          </form>
          <div className="list-group mt-5">
          {tasks.map(task =>
              <div className="list-group-item" key={task.uuid}>
                  <div className="d-flex flex-row align-items-center">
                      <div className="flex-grow-1">{task.name}</div>
                      <div>
                          <button type="button"
                                  className="btn btn-danger btn-sm"
                                  onClick={() => onTaskDelete(task.uuid)}>
                              delete
                          </button>
                      </div>
                  </div>
              </div>
          )}
          </div>
      </div>
  )
}

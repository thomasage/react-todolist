import Dexie from 'dexie'

const db = new Dexie('todo')
db.version(1).stores({
  tasks: 'uuid,name'
})

export default db
